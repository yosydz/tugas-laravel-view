<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Faker\Factory as Faker;

class MainController extends Controller
{
    public function dummyData()
    {
        $faker = Faker::create('id_ID');
        return [
            'nama' => $faker->name,
            'nim' => $faker->numerify('##########'),
            // 'email' => $faker->valid()->email,
            'telp' => $faker->phoneNumber,
            'alamat' => $faker->address
        ];
    }

    public function index(){

        for($i = 1; $i <= 20; $i++){
            $datas[] = $this->dummyData();
        };

        // dd($mahasiswa);
        return view('index', compact('datas'));
    }
}
