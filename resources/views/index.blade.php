@extends('layouts.app')
@section('content')
<div class="content">
    <div class="container">
        <div class="row">
            <h1>Data Mahasiswa</h1>
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Nama</th>
                    <th scope="col">NIM</th>
                    <th scope="col">No. Telp</th>
                    <th scope="col">Alamat</th>
                </tr>
                </thead>
                @foreach($datas as $data)
                        <tbody>
                        <tr>
                            <th scope="row">{{ $loop->iteration }}</th>
                            <td>{{ $data['nama'] }}</td>
                            <td>{{ $data['nim'] }}</td>
                            <td>{{ $data['telp'] }}</td>
                            <td>{{ $data['alamat'] }}</td>
                        </tr>
                        </tbody>
                @endforeach
            </table>
        </div>
    </div>
</div>
@endsection